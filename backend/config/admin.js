module.exports = ({ env }) => ({
  auth: {
    secret: env('ADMIN_JWT_SECRET', 'd19d98aa382f44998349de6b01637d4d'),
  },
});
