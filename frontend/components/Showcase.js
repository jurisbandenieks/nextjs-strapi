import syles from "@/styles/Showcase.module.css";

const Showcase = () => {
  return (
    <div className={syles.showcase}>
      <h1>Welcome to the part!</h1>
      <h2>Find the hottest DJ events</h2>
    </div>
  );
};

export default Showcase;
