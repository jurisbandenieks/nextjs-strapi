import Layout from "@/components/Layout";
import Link from "next/link";
import EventItem from "@/components/EventItem";
import { API_URL } from "@/config/index";
import qs from "qs";

export default function HomePage({ events }) {
  return (
    <Layout>
      <h1>Upcoming Events</h1>
      {events.length === 0 && <h3>No events to show</h3>}
      {!!events.length &&
        events.map((evt) => <EventItem key={evt.id} evt={evt.attributes} />)}

      {events.length > 0 && (
        <Link href="/events">
          <a className="btn-secondary">View All Events</a>
        </Link>
      )}
    </Layout>
  );
}

export async function getStaticProps() {
  const query = qs.stringify(
    {
      populate: "*",
      sort: ["date:asc"],
      pagination: {
        start: 0,
        limit: 3
      }
    },
    {
      encodeValuesOnly: true
    }
  );

  const res = await fetch(`${API_URL}/api/events?${query}`);
  const { data } = await res.json();

  return {
    props: { events: data },
    revalidate: 1
  };
}
