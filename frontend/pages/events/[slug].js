import { parseCookies } from "@/helpers/index";
import Layout from "@/components/Layout";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Link from "next/link";
import Image from "next/image";
import { API_URL } from "@/config/index";
import styles from "@/styles/Event.module.css";
import qs from "qs";

const EventPage = ({ evt }) => {
  const item = evt.attributes;

  return (
    <Layout>
      <div className={styles.event}>
        <span>
          {new Date(item.date).toLocaleDateString("en-US")} at {item.time}
        </span>
        <h1>{item.name}</h1>
        <ToastContainer />
        {item.image?.data && (
          <div className={styles.image}>
            <Image
              src={item.image.data.attributes.formats.medium.url}
              width={960}
              height={600}
            />
          </div>
        )}

        <h3>Performers:</h3>
        <p>{item.performers}</p>
        <h3>Description:</h3>
        <p>{item.description}</p>
        <h3>Venue: {item.venue}</h3>
        <p>{item.address}</p>

        <Link href="/events">
          <a className={styles.back}>{"<"} Go Back</a>
        </Link>
      </div>
    </Layout>
  );
};

export default EventPage;

// export async function getStaticPaths() {
//   const res = await fetch(`${API_URL}/api/events`);
//   const events = await res.json();

//   const paths = events.map((evt) => ({
//     params: { slug: evt.slug }
//   }));

//   return { paths, fallback: true };
// }

// export async function getStaticProps({ params: { slug } }) {
//   const res = await fetch(`${API_URL}/api/events/${slug}`);
//   const events = await res.json();

//   return { props: { evt: events[0] }, revalidate: 1 };
// }

export async function getServerSideProps({ query: { slug } }) {
  const query = qs.stringify(
    {
      populate: "*",
      filters: { slug: { $eq: slug } }
    },
    {
      encodeValuesOnly: true
    }
  );
  const res = await fetch(`${API_URL}/api/events?${query}`);
  const { data } = await res.json();
  const evt = data[0];

  return { props: { evt } };
}
