import Layout from "@/components/Layout";
import EventItem from "@/components/EventItem";
import Pagination from "@/components/Pagination";
import { API_URL, PER_PAGE } from "@/config/index";
import qs from "qs";

export default function EventsPage({ events, page, total }) {
  return (
    <Layout>
      <h1>Events</h1>
      {events.length === 0 && <h3>No events to show</h3>}
      {events.map((evt) => (
        <EventItem key={evt.id} evt={evt.attributes} />
      ))}

      <Pagination page={page} total={total} />
    </Layout>
  );
}

export async function getServerSideProps({ query: { page = 1 } }) {
  const query = qs.stringify(
    {
      populate: "*",
      sort: ["date:asc"],
      pagination: {
        page,
        pageSize: PER_PAGE
      }
    },
    {
      encodeValuesOnly: true
    }
  );

  const res = await fetch(`${API_URL}/api/events?${query}`);
  const {
    data,
    meta: { pagination }
  } = await res.json();

  return {
    props: {
      events: data,
      page: Number(pagination.page),
      total: pagination.total
    }
  };
}
